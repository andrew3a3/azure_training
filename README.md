# Azure Functions

## Bindings
1. *Queue*
2. *Cosmos DB*
3. *Blob Storage*
4. *Service Bus*
5. *Table Storage*
6. *SendGrid*

## Hosting Models
1. *Consumption Plan*: Per-Second Billing, 1,000,000 executions, 400,000 GB-s
2. *App Service Plan*: Reserved, servers, Predictable monthly cost
3. *Premium Plan*: Pre-warmed Instances, vNet integration, Longer run duration
4. *Docker Container*: Run anywhere, on premises, other cloud providers

## Additional Features
1. *Security*: API keys, identity provider integration
2. *Durable Functions*: Define workflows, run tasks in parallel, retries and error handling
3. *Proxies*: Route incoming requests, static websites, transform requests and responses

# Function Apps
Units of deployment, share common configuration, scale together, logically related